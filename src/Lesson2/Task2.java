package Lesson2;

public class Task2 {
    public static void main(String[] args) {
        int num = 0;
        while (num < 100) {
            if (num % 3 == 0 && num % 7 == 0) {
                System.out.println(num);
            }else {
                break;
            }
            num++;
        }
    }
}
