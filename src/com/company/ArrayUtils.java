package com.company;

import java.util.Random;
import java.util.Scanner;

import static com.company.StringUtils.LINE_FEED;
import static com.company.StringUtils.SPACE;

public class ArrayUtils {

    private static final Random RANDOM = new Random();
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final String USER_INPUT_MESSAGE = "Введите элементы массива";

    public static int[] createArrayFromRandomElements(int elementsAmount, int bound) {

        int[] array = new int[elementsAmount];
        for (int i = 0; i < array.length; i++) {
            array[i] = RANDOM.nextInt(bound);
        }
        return array;
    }

    public static int[] createArrayFromUserInput(int elementsAmount) {

        System.out.println(USER_INPUT_MESSAGE);

        int[] array = new int[elementsAmount];
        for (int i = 0; i < array.length; i++) {
            array[i] = SCANNER.nextInt();
        }
        return array;
    }

    public static void printArrayInLine(int[] array) {

        for (int value : array) {
            System.out.print(value + SPACE);
        }
        System.out.println(LINE_FEED);
    }

    public static void printArrayInColumn(int[] array) {

        for (int value : array) {
            System.out.println(value);
        }
    }
}
