package com.company;

public class EndingUtils {

    private static final String ENDING_OV = "ов";
    private static final String ENDING_ZERO = "";
    private static final String ENDING_A = "а";

    public static String getEnding(int count) {

        if (count > 10 && count < 15) {
            return ENDING_OV;
        }

        if (count % 10 == 1) {
            return ENDING_ZERO;
        } else if (count % 10 == 2
                || count % 10 == 3
                || count % 10 == 4) {
            return ENDING_A;
        } else {
            return ENDING_OV;
        }
    }
}

