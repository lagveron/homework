package com.company;

import java.util.Random;
import java.util.Scanner;

public class MatrixUtils {

    private static final Random RANDOM = new Random();
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final String USER_INPUT_MESSAGE = "Введите количество строк и столбцов в массиве";

    public static int[][] createMatrix() {

        System.out.println(USER_INPUT_MESSAGE);

        int rowCount = SCANNER.nextInt();
        int columnCount = SCANNER.nextInt();
        int[][] matrix = new int[rowCount][columnCount];

        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                matrix[i][j] = RANDOM.nextInt(51);
            }
        }
        return matrix;
    }

    public static void printMatrix(int[][] matrix) {
        for (int[] rows : matrix) {
            for (int element : rows) {
                System.out.print(element + StringUtils.TAB);
            }
            System.out.println();
        }
    }
}
