package homework.lesson3.matrix;

import static com.company.MatrixUtils.createMatrix;
import static com.company.MatrixUtils.printMatrix;
import static com.company.StringUtils.LINE_FEED;
import static com.company.StringUtils.TAB;

public class InvertMatrixPrinter {

    public static void main(String[] args) {

        int[][] matrix = createMatrix();
        printMatrix(matrix);
        System.out.println(LINE_FEED);
        printInvertMatrix(matrix);
    }

    private static void printInvertMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[j][i] + TAB);
            }
            System.out.println();
        }
    }
}
