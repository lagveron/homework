package homework.lesson3.matrix;

import static com.company.MatrixUtils.createMatrix;
import static com.company.MatrixUtils.printMatrix;

/**
 * Task 2
 */
public class OddNumbersBelowDiagonalCounter {

    public static void main(String[] args) {

        int[][] matrix = createMatrix();
        printMatrix(matrix);
        System.out.println(getOddNumbersBelowDiagonalSum(matrix));

    }

    private static int getOddNumbersBelowDiagonalSum(int[][] matrix) {

        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < i + 1; j++) {
                if (matrix[i][j] % 2 != 0) {
                    sum += matrix[i][j];
                }
            }
        }
        return sum;
    }
}

