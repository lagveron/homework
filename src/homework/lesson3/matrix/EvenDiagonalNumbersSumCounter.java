package homework.lesson3.matrix;

import static com.company.MatrixUtils.createMatrix;
import static com.company.MatrixUtils.printMatrix;

/**
 * Task 1
 */
public class EvenDiagonalNumbersSumCounter {

    public static void main(String[] args) {
        int[][] matrix = createMatrix();
        printMatrix(matrix);
        System.out.println(getEvenDiagonalNumbersSum(matrix));

    }

    private static int getEvenDiagonalNumbersSum(int[][] matrix) {

        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i][i] % 2 == 0) {
                sum += matrix[i][i];
            }
        }
        return sum;
    }
}


