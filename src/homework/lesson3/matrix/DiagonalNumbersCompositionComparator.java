package homework.lesson3.matrix;

import static com.company.MatrixUtils.createMatrix;
import static com.company.MatrixUtils.printMatrix;

/**
 * Task 3
 */
public class DiagonalNumbersCompositionComparator {

    private static final String FIRST_GRATER_THAN_SECOND = "Произведение элементов первой диагонали больше";
    private static final String SECOND_GRATER_THAN_FIRST = "Произведение элементов второй диагонали больше";
    private static final String FIRST_EQUALS_SECOND = "Произведения элементов двух диагоналей равны";

    public static void main(String[] args) {

        int[][] matrix = createMatrix();
        int firstDiagonalComposition = getFirstDiagonalNumbersComposition(matrix);
        int secondDiagonalComposition = getSecondDiagonalNumbersComposition(matrix);

        printMatrix(matrix);
        System.out.println(getFirstDiagonalNumbersComposition(matrix));
        System.out.println(getSecondDiagonalNumbersComposition(matrix));
        System.out.println(compareDiagonalsComposition(firstDiagonalComposition, secondDiagonalComposition));
    }

    private static int getFirstDiagonalNumbersComposition(int[][] matrix) {

        int composition = 1;
        for (int i = 0; i < matrix.length; i++) {
            composition *= matrix[i][i];
        }
        return composition;
    }

    private static int getSecondDiagonalNumbersComposition(int[][] matrix) {

        int composition = 1;
        for (int i = 0; i < matrix.length; i++) {
            composition *= matrix[i][matrix.length - 1 - i];
        }
        return composition;
    }

    public static String compareDiagonalsComposition(int firstDiagonalComposition, int secondDiagonalComposition) {
        if (firstDiagonalComposition > secondDiagonalComposition) {
            return FIRST_GRATER_THAN_SECOND;
        } else if (firstDiagonalComposition < secondDiagonalComposition) {
            return SECOND_GRATER_THAN_FIRST;
        } else {
            return FIRST_EQUALS_SECOND;
        }
    }
}
