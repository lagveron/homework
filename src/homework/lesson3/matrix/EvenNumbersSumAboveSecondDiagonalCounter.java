package homework.lesson3.matrix;

import com.company.MatrixUtils;

public class EvenNumbersSumAboveSecondDiagonalCounter {

    public static void main(String[] args) {

        int[][] matrix = MatrixUtils.createMatrix();
        MatrixUtils.printMatrix(matrix);
        System.out.println(getEvenNumbersAboveSecondDiagonalSum(matrix));

    }

    private static int getEvenNumbersAboveSecondDiagonalSum(int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length - i - 1; j++) {
                if (matrix[i][j] % 2 == 0) {
                    sum += matrix[i][j];
                }
            }
        }
        return sum;
    }
}

