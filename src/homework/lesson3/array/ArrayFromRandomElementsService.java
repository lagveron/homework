package homework.lesson3.array;

import com.company.ArrayUtils;

import java.util.Arrays;

import static com.company.ArrayUtils.*;
import static com.company.ArrayUtils.createArrayFromRandomElements;

/**
 * Task 4, Task 6, Task 7
 */
public class ArrayFromRandomElementsService {

    private static final String INCREASING_SEQUENCE = "Последовательность строго возрастающая";
    private static final String NON_INCREASING_SEQUENCE = "Последовательность не строго возрастающая";

    public static void main(String[] args) {
        // Task 4
        int[] arrayForTask4 = createArrayFromRandomElements(15, 21);
        System.out.println(Arrays.toString(arrayForTask4));
        System.out.println(Arrays.toString(replaceOddIndexWithZero(arrayForTask4)));

        // Task 6
        int[] arrayForTask6 = createArrayFromRandomElements(4, 11);
        System.out.println(Arrays.toString(arrayForTask6));
        System.out.println(getSequenceKind(arrayForTask6));

        //Task7
        int[] array = createArrayFromRandomElements(12, 16);
        System.out.println(Arrays.toString(array));
        System.out.println(getMaxElementIndex(array));
    }

    private static int[] replaceOddIndexWithZero(int[] array) {
        for (int j = 0; j < array.length; j++) {
            if (j % 2 != 0) {
                array[j] = 0;
            }
        }
        return array;
    }

    private static boolean isIncreasingSequence(int[] array) {
        boolean isIncreasing = true;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                isIncreasing = false;
                break;
            }
        }
        return isIncreasing;
    }

    private static String getSequenceKind(int[] array) {
        return isIncreasingSequence(array) ? INCREASING_SEQUENCE : NON_INCREASING_SEQUENCE;
    }

    private static int getMaxElementIndex(int[] array) {
        int max = 0;
        int index = 0;
        for (int i = 1; i < array.length - 1; i++) {
            if (max < array[i]) {
                max = array[i];
                index = i;
            }
        }
        return index;
    }
}
