package homework.lesson3.array;

import static com.company.ArrayUtils.printArrayInLine;
import static com.company.StringUtils.SPACE;

/**
 * Task2
 */
public class OddArrayService {

    private static final int STEP = 2;

    public static void main(String[] args) {

        printArrayInLine(createOddElementsArray(50));

        printReversedOrderArray(createOddElementsArray(50));
    }

    private static void printReversedOrderArray(int[] array) {

        for (int i = 1; i <= array.length; i++) {
            System.out.print(array[array.length - i] + SPACE);
        }
    }

    private static int[] createOddElementsArray(int elementsAmount) {

        int[] array = new int[elementsAmount];
        int element = 1;

        for (int i = 0; i < array.length; i++) {
            array[i] = element;
            element += STEP;
        }
        return array;
    }
}
