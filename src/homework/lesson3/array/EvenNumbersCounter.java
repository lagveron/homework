package homework.lesson3.array;

import static com.company.ArrayUtils.createArrayFromRandomElements;
import static com.company.ArrayUtils.printArrayInLine;

/**
 * Task3
 */
public class EvenNumbersCounter {

    public static void main(String[] args) {

        int[] array = createArrayFromRandomElements(15, 100);

        printArrayInLine(array);

        System.out.println(getEvenNumbersCount(array));
    }

    private static int getEvenNumbersCount(int[] array) {
        int count = 0;
        for (int value : array) {
            if (value % 2 == 0) {
                count++;
            }
        }
        return count;
    }
}
