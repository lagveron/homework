package homework.lesson3.array;

import com.company.ArrayUtils;

import java.util.Scanner;

/**
 * Task 9
 */
public class HalfOfSumArrayPrinter {

    public static final String FIRST_GRATER_THAN_SECOND = "Сумма левой половины массива больше суммы правой";
    public static final String SECOND_GRATER_THAN_FIRST = "Сумма правой половины массива больше суммы левой";
    public static final String FIRST_EQUALS_SECOND = "Суммы модулей равны";
    private static final String USER_INPUT_MESSAGE = "Введите количество элементов массива";

    public static void main(String[] args) {

        System.out.println(USER_INPUT_MESSAGE);
        Scanner scanner = new Scanner(System.in);
        int arrayElementsCount = scanner.nextInt();
        int[] array = ArrayUtils.createArrayFromRandomElements(arrayElementsCount, 16);

        int mid = array.length / 2;
        int leftHalfSum;
        int rightHalfSum;

        if (arrayElementsCount < 0) {
            System.out.println("Вы ввели отрицательное число");
            return;
        }
        leftHalfSum = getHalfSum(array, 0, mid);
        rightHalfSum = getHalfSum(array, mid, array.length);
        ArrayUtils.printArrayInLine(array);
        System.out.println(compareHalvesSum(leftHalfSum, rightHalfSum));
    }

    private static int getHalfSum(int[] array, int startIndex, int endIndex) {
        int halfSum = 0;
        for (int i = startIndex; i < endIndex; i++) {
            halfSum += array[i];
        }
        return halfSum;
    }

    private static String compareHalvesSum(int leftHalfSum, int rightHalfSum) {
        if (leftHalfSum > rightHalfSum) {
            return FIRST_GRATER_THAN_SECOND;
        } else if (leftHalfSum < rightHalfSum) {
            return SECOND_GRATER_THAN_FIRST;
        } else {
            return FIRST_EQUALS_SECOND;
        }
    }
}
