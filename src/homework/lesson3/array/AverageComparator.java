package homework.lesson3.array;

import static com.company.ArrayUtils.createArrayFromRandomElements;

/**
 *Task5
 */
public class AverageComparator {

    private static final String FIRST_GRATER_THAN_SECOND = "Среднее арифметическое первого массива больше, чем второго";
    private static final String SECOND_GRATER_THAN_FIRST = "Среднее арифметическое второго массива больше, чем первого";
    private static final String FIRST_EQUALS_SECOND = "Средние арифметические двух массивов равны";

    public static void main(String[] args) {

        int[] firstArray = createArrayFromRandomElements(5, 16);
        int[] secondArray = createArrayFromRandomElements(5, 16);
        double firstArrayAverage = getArrayAverage(firstArray);
        double secondArrayAverage = getArrayAverage(secondArray);

        System.out.println(firstArrayAverage);
        System.out.println(secondArrayAverage);
        System.out.println(compareAverage(firstArrayAverage, secondArrayAverage));
    }

    private static double getArrayAverage(int[] array) {
        double arrayAverage;
        double arraySumElements = 0;
        for (int value : array) {
            arraySumElements += value;
        }
        arrayAverage = arraySumElements / array.length;
        return arrayAverage;
    }

    private static String compareAverage(double firstArrayAverage, double secondArrayAverage) {
        if (firstArrayAverage > secondArrayAverage) {
            return (FIRST_GRATER_THAN_SECOND);
        } else if ((firstArrayAverage < secondArrayAverage)) {
            return SECOND_GRATER_THAN_FIRST;
        } else {
            return FIRST_EQUALS_SECOND;
        }
    }
}
