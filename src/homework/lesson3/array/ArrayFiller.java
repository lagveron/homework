package homework.lesson3.array;

import com.company.ArrayUtils;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Task 10
 */
public class ArrayFiller {

    private static final String USER_INPUT_MESSAGE = "Введите положительное число >3";

    public static void main(String[] args) {

        System.out.println(USER_INPUT_MESSAGE);
        Scanner scanner = new Scanner(System.in);
        int arrayElementsCount = scanner.nextInt();
        int[] array = ArrayUtils.createArrayFromRandomElements(arrayElementsCount, arrayElementsCount);

        if (arrayElementsCount < 3) {
            System.out.println("Вы ввели число<3");
            return;
        }

        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(createAndFillArrayWithEvenNumbers(array)));
    }

    private static int[] createAndFillArrayWithEvenNumbers(int[] array) {
        int count = getEvenNumbersCount(array);
        int[] resultArray = new int[count];
        int element = 0;
        for (int value : array) {
            if (value % 2 == 0 && value != 0) {
                resultArray[element] = value;
                element++;
            }
        }
        return resultArray;
    }

    private static int getEvenNumbersCount(int[] array) {
        int count = 0;
        for (int value : array) {
            if (value % 2 == 0 && value!=0) {
                count++;
            }
        }
        return count;
    }
}
