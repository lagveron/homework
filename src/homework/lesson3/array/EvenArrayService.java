package homework.lesson3.array;

import com.company.ArrayUtils;

import static com.company.ArrayUtils.printArrayInColumn;
import static com.company.ArrayUtils.printArrayInLine;

/**
 * Task1
 */
public class EvenArrayService {

    private static final int STEP = 2;

    public static void main(String[] args) {

        printArrayInLine(createEvenElementsArray(10));
        printArrayInColumn(createEvenElementsArray(10));
    }

    private static int[] createEvenElementsArray(int elementsAmount) {
        int[] array = new int[elementsAmount];
        int element = STEP;
        for (int i = 0; i < array.length; i++) {
            array[i] = element;
            element += STEP;
        }
        return array;
    }


}

