package homework.lesson3.array;

import java.util.Arrays;

import static com.company.ArrayUtils.createArrayFromRandomElements;

/**
 * Task 8
 */
public class ArrayFillerAndIntegerCounter {

    public static void main(String[] args) {

        int[] firstArray = createArrayFromRandomElements(10, 10);
        int[] secondArray = createArrayFromRandomElements(10, 10);
        double[] resultArray = createAndFillArray(firstArray, secondArray);

        System.out.println(Arrays.toString(firstArray));
        System.out.println(Arrays.toString(secondArray));
        System.out.println(Arrays.toString(resultArray));
        System.out.println(getIntegersCount(resultArray));
    }

    private static double[] createAndFillArray(int[] firstArray, int[] secondArray) {
        double[] resultArray = new double[10];
        
        for (int i = 0; i < resultArray.length; i++) {
            if (secondArray[i] == 0) {
                resultArray[i] = Integer.MAX_VALUE;
            } else {
                resultArray[i] = (double) firstArray[i] / (double) secondArray[i];
            }
        }
        return resultArray;
    }

    private static int getIntegersCount(double[] array) {
        int count = 0;
        for (double value : array) {
            if (value % 1 == 0) {
                count++;
            }
        }
        return count;
    }
}
