package homework.lesson2;

import com.company.EndingUtils;

public class AmoebaCounter {

    public static void main(String[] args) {
        int amoebaCount = 11;
        for (int i = 0; i <= 24; i += 3) {
            amoebaCount *= 2;
            System.out.println("Количество амеб через " + i + " час" + EndingUtils.getEnding(i) + " - " + amoebaCount);
        }
    }
}
