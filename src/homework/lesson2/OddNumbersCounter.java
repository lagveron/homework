package homework.lesson2;

public class OddNumbersCounter {

    public static void main(String[] args) {

        System.out.println(countOddNumbersSum());
    }

    private static int countOddNumbersSum() {
        int i = 1;
        int sum = 0;
        while (i < 100) {
            if (i % 2 != 0) {
                sum += i;
            }
            i++;
        }
        return sum;
    }
}

