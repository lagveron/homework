package homework.lesson2;

public class StarsPrinter {

    private static final String STAR = "*";

    public static void main(String[] args) {
        printStars();
        printStars2();
    }

    private static void printStars() {
        String stars = "";
        for (int i = 0; i < 4; i++) {
            stars += STAR;
            System.out.println(stars);
        }
    }

    private static void printStars2() {
        for (int i = 4; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                System.out.print(STAR);
            }
            System.out.println();
        }
    }
}

