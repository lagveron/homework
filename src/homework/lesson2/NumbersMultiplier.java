package homework.lesson2;

public class NumbersMultiplier {

    public static void main(String[] args) {

        System.out.println(getComposition(3, 5));
    }

    private static int getComposition(int a, int b) {
        int c = 0;
        for (int i = 0; i < a; i++) {
            c += b;
        }
        return c;
    }
}