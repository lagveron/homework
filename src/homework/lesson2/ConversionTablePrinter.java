package homework.lesson2;

public class ConversionTablePrinter {

    private static final double COEFFICIENT = 2.54;

    public static void main(String[] args) {
        printConversionTable();
    }

    private static void printConversionTable() {
        double inch;
        for (int i = 1; i <= 20; i++) {
            inch = i * COEFFICIENT;
            System.out.println(i + " sm = " + inch + " inch");
        }
    }
}
