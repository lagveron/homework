package homework.lesson2;

public class DistanceCounter {

    public static void main(String[] args) {
        System.out.println(countDistanceForSevenDays(10.0));
    }

    private static double countDistanceForSevenDays(double distance) {
        double sum = 0;
        for (int i = 1; i < 7; i++) {
            distance += (distance * 0.1);
            sum += distance;
        }
        return sum;
    }
}
