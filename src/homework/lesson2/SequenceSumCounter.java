package homework.lesson2;

public class SequenceSumCounter {

    public static void main(String[] args) {
        System.out.println(getSequenceSum());
    }

    private static int getSequenceSum() {
        int sum = 0;
        for (int i = 1; i <= 256; i *= 2) {
            sum += i;
        }
        return sum;
    }

}
