package homework.lesson5.robot.hand;

public class SamsungHand implements IHand {

    private int price;

    public SamsungHand() {
    }

    public SamsungHand(int price) {
        this.price = price;
    }

    @Override
    public void upHand() {
        System.out.println("поднимается рука Samsung");
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "SamsungHand{" +
                "price=" + price +
                '}';
    }
}
