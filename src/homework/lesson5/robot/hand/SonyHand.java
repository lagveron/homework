package homework.lesson5.robot.hand;

public class SonyHand implements IHand {

    private int price;

    public SonyHand() {
    }

    public SonyHand(int price) {
        this.price = price;
    }

    @Override
    public void upHand() {
        System.out.println("поднимается рука Sony");
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "SonyHand{" +
                "price=" + price +
                '}';
    }
}
