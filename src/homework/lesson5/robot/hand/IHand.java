package homework.lesson5.robot.hand;

public interface IHand {

    void upHand();

    int getPrice();
}
