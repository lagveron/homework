package homework.lesson5.robot.hand;

public class ToshibaHand implements IHand {

    private int price;

    public ToshibaHand(int price) {
        this.price = price;
    }

    public ToshibaHand() {
    }

    @Override
    public void upHand() {
        System.out.println("поднимается рука Toshiba");
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "ToshibaHand{" +
                "price=" + price +
                '}';
    }
}
