package homework.lesson5.robot;

import homework.lesson5.robot.hand.SamsungHand;
import homework.lesson5.robot.hand.SonyHand;
import homework.lesson5.robot.hand.ToshibaHand;
import homework.lesson5.robot.head.SamsungHead;
import homework.lesson5.robot.head.SonyHead;
import homework.lesson5.robot.head.ToshibaHead;
import homework.lesson5.robot.leg.SamsungLeg;
import homework.lesson5.robot.leg.SonyLeg;
import homework.lesson5.robot.leg.ToshibaLeg;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        IRobot robot1 = new Robot(new SamsungHead(5), new SonyHand(10), new ToshibaLeg(8));
        IRobot robot2 = new Robot(new SonyHead(12), new ToshibaHand(3), new SamsungLeg(6));
        IRobot robot3 = new Robot(new ToshibaHead(4), new SamsungHand(7), new SonyLeg(11));

        robot1.action();
        robot2.action();
        robot3.action();

        List<IRobot> robots = List.of(robot1, robot2, robot3).stream()
                .sorted(Comparator.comparingInt(IRobot::getPrice))
                .collect(Collectors.toList());

        if (robots.get(2) == robots.get(0)) {
            System.out.println("Стоимость роботов одинаковая");
        } else if (robots.get(2) == robots.get(1)) {
            System.out.println("Стоимость роботов " + robots.get(2) + " " + robots.get(1) + "одинаковая");
        } else {
            System.out.println("Самый дорогой робот - " + robots.get(2));
        }
    }
}
