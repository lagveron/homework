package homework.lesson5.robot.leg;

import homework.lesson5.robot.leg.ILeg;

public class SamsungLeg implements ILeg {

    private int price;

    public SamsungLeg() {
    }

    public SamsungLeg(int price) {
        this.price = price;
    }

    @Override
    public void step() {
        System.out.println("шагает нога Samsung");
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "SamsungLeg{" +
                "price=" + price +
                '}';
    }
}
