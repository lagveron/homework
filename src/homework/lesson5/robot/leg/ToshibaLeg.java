package homework.lesson5.robot.leg;

import homework.lesson5.robot.leg.ILeg;

public class ToshibaLeg implements ILeg {

    private int price;

    public ToshibaLeg() {
    }

    public ToshibaLeg(int price) {
        this.price = price;
    }

    @Override
    public void step() {
        System.out.println("шагает нога Toshiba");
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "ToshibaLeg{" +
                "price=" + price +
                '}';
    }
}
