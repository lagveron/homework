package homework.lesson5.robot.leg;

public interface ILeg {

    void step();

    int getPrice();

}
