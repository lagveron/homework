package homework.lesson5.robot.leg;

public class SonyLeg implements ILeg {

    private int price;

    public SonyLeg() {
    }

    public SonyLeg(int price) {
        this.price = price;
    }

    @Override
    public void step() {
        System.out.println("шагает нога Sony");
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "SonyLeg{" +
                "price=" + price +
                '}';
    }
}
