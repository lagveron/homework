package homework.lesson5.robot.head;

public class ToshibaHead implements IHead {

    private int price;

    public ToshibaHead() {
    }

    public ToshibaHead(int price) {
        this.price = price;
    }

    @Override
    public void speak() {
        System.out.println("говорит голова Toshiba");

    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "ToshibaHead{" +
                "price=" + price +
                '}';
    }
}
