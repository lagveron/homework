package homework.lesson5.robot.head;

public interface IHead {

    void speak();

    int getPrice();
}
