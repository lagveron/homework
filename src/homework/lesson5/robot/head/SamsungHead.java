package homework.lesson5.robot.head;

public class SamsungHead implements IHead {

    private int price;

    public SamsungHead() {
    }

    public SamsungHead(int price) {
        this.price = price;
    }

    @Override
    public void speak() {
        System.out.println("говорит голова Samsung");
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "SamsungHead{" +
                "price=" + price +
                '}';
    }
}
