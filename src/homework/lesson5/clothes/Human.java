package homework.lesson5.clothes;

import homework.lesson5.clothes.jacket.Coat;
import homework.lesson5.clothes.shoe.Sneaker;
import homework.lesson5.clothes.trousers.Jeans;

public class Human {

    private String name;
    private Coat coat;
    private Jeans jeans;
    private Sneaker sneaker;

    public Human(String name, Coat coat, Jeans jeans, Sneaker sneaker) {
        this.name = name;
        this.coat = coat;
        this.jeans = jeans;
        this.sneaker = sneaker;
    }

    public void getDressed() {
        coat.putOn();
        jeans.putOn();
        sneaker.putOn();
    }

    public void getUndressed() {
        coat.takeOff();
        jeans.takeOff();
        sneaker.takeOff();
    }

}
