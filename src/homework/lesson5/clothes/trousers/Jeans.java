package homework.lesson5.clothes.trousers;

public class Jeans implements Trousers {

    @Override
    public void putOn() {
        System.out.println("Джинсы надеты");
    }

    @Override
    public void takeOff() {
        System.out.println("Джинсы сняты");
    }
}
