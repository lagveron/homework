package homework.lesson5.clothes.trousers;

public interface Trousers {

    void putOn();

    void takeOff();

}
