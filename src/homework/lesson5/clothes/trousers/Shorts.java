package homework.lesson5.clothes.trousers;

public class Shorts implements Trousers {
    @Override
    public void putOn() {
        System.out.println("Шорты надеты");
    }

    @Override
    public void takeOff() {
        System.out.println("Шорты сняты");
    }
}
