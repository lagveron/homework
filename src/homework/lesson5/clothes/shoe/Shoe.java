package homework.lesson5.clothes.shoe;

public interface Shoe {

    void putOn();

    void takeOff();

}
