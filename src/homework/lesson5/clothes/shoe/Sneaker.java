package homework.lesson5.clothes.shoe;

public class Sneaker implements Shoe {

    public Sneaker() {
    }

    @Override
    public void putOn() {
        System.out.println("Кроссовки надеты");
    }

    @Override
    public void takeOff() {
        System.out.println("Кроссовки сняты");
    }
}
