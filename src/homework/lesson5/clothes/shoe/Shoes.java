package homework.lesson5.clothes.shoe;

public class Shoes implements Shoe {

    @Override
    public void putOn() {
        System.out.println("Туфли надеты");
    }

    @Override
    public void takeOff() {
        System.out.println("Туфли сняты");
    }
}
