package homework.lesson5.clothes.jacket;

public interface Jacket {

    void putOn();

    void takeOff();

}
