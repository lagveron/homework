package homework.lesson5.clothes.jacket;

public class Coat implements Jacket{

    public Coat() {
    }

    @Override
    public  void putOn() {
        System.out.println("Пальто надето");
    }

    @Override
    public void takeOff() {
        System.out.println("Пальто снято");
    }
}
