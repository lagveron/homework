package homework.lesson5;

import homework.lesson5.clothes.Human;
import homework.lesson5.clothes.jacket.Coat;
import homework.lesson5.clothes.shoe.Sneaker;
import homework.lesson5.clothes.trousers.Jeans;
import homework.lesson5.spacePort.Rocket;
import homework.lesson5.spacePort.Shuttle;
import homework.lesson5.spacePort.SpacePort;

public class Main {
    public static void main(String[] args) {


        // clothes
        Human human = new Human("Gena", new Coat(), new Jeans(), new Sneaker());

        human.getDressed();

        human.getUndressed();

        // spaceport
        SpacePort spacePort = new SpacePort();
        spacePort.launch(new Rocket());
        spacePort.launch(new Shuttle());

    }
}
