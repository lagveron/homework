package homework.lesson5.spacePort;

import java.util.Random;

public class Shuttle implements IStart {

    private static final Random random = new Random();
    private static final String SUCCESS_CHECK_MESSAGE = "Предстартовая проверка прошла успешно";
    private static final String FAILED_CHECK_MESSAGE = "Предстартовая проверка провалена";
    private static final String START_ENGINES_MESSAGE = "Двигатель запущен";
    private static final String START_MESSAGE = "Старт осуществлен";

    @Override
    public boolean checkSystems() {
        if (random.nextInt(11) > 3) {
            System.out.println(SUCCESS_CHECK_MESSAGE);
            return true;
        } else {
            System.out.println(FAILED_CHECK_MESSAGE);
            return false;
        }
    }

    @Override
    public void startEngines() {
        System.out.println(START_ENGINES_MESSAGE);
    }

    @Override
    public void start() {
        System.out.println(START_MESSAGE);
    }
}
