package homework.lesson5.spacePort;

public class SpacePort {

    public SpacePort() {
    }

    public void launch(IStart rocket) {
        if (rocket.checkSystems()) {
            rocket.startEngines();
            for (int i = 10; i >= 0; i--) {
                System.out.println(i);
            }
            rocket.start();
        }
    }
}
