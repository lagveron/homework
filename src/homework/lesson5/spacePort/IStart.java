package homework.lesson5.spacePort;

public interface IStart {

    boolean checkSystems();

    void startEngines();

    void start();

}
