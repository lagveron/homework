package homework.lesson4;

import java.util.Random;
import java.util.Scanner;

public class Computer {

    private static final Random RANDOM = new Random();
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final String TURN_ON_MESSAGE = "Computer is turned on";
    private static final String TURN_OFF_MESSAGE = "Computer is turned off";
    private static final String BURNED_OUT_MESSAGE = "Computer burned out";
    private static final String USER_INPUT_MESSAGE = "Enter the number 0 or 1";

    private String cpu;
    private int ram;
    private Integer hdd;
    private int fullLifeCycle;
    private boolean computerBurned;
    private int countLifeCycle;

    public Computer(String cpu, int ram, Integer hdd, int fullLifeCycle, boolean computerBurned) {
        this.cpu = cpu;
        this.ram = ram;
        this.hdd = hdd;
        this.fullLifeCycle = fullLifeCycle;
        this.computerBurned = computerBurned;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public Integer getHdd() {
        return hdd;
    }

    public void setHdd(Integer hdd) {
        this.hdd = hdd;
    }

    public int getFullLifeCycle() {
        return fullLifeCycle;
    }

    public void setFullLifeCycle(int fullLifeCycle) {
        this.fullLifeCycle = fullLifeCycle;
    }

    public boolean isComputerBurned() {
        return computerBurned;
    }

    public void setComputerBurned(boolean computerBurned) {
        this.computerBurned = computerBurned;
    }

    public int getCountLifeCycle() {
        return countLifeCycle;
    }

    public void setCountLifeCycle(int countLifeCycle) {
        this.countLifeCycle = countLifeCycle;
    }

    public void incrementCountLifeCycle(){
        countLifeCycle++;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "cpu='" + cpu + '\'' +
                ", ram=" + ram +
                ", hdd=" + hdd +
                ", fullLifeCycle=" + fullLifeCycle +
                '}';
    }

    public String turnOnComputer(Computer computer) {

        if (computer.isComputerBurned()) {
            return BURNED_OUT_MESSAGE;
        }

        System.out.println(USER_INPUT_MESSAGE);

        if (RANDOM.nextInt(2) != SCANNER.nextInt()) {
            return (BURNED_OUT_MESSAGE);
        } else {
            computer.incrementCountLifeCycle();
            return (TURN_ON_MESSAGE);
        }
    }

    public String turnOffComputer(Computer computer) {

        if (computer.isComputerBurned()) {
            return BURNED_OUT_MESSAGE;
        }

        System.out.println(USER_INPUT_MESSAGE);

        if (RANDOM.nextInt(2) != SCANNER.nextInt()) {
            return (BURNED_OUT_MESSAGE);
        } else {
            return (TURN_OFF_MESSAGE);
        }
    }
}

