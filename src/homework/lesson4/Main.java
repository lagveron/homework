package homework.lesson4;

public class Main {
    public static void main(String[] args) {

        Computer computer = new Computer("Intel core i7", 32, 1, 15000, false);

        System.out.println(computer.turnOnComputer(computer));
        System.out.println(computer.turnOffComputer(computer));
        System.out.println(computer.toString());
    }
}

