package homework.lesson6;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class Task3_Main {
    public static void main(String[] args) throws Exception {

        //Task 3 - В исходном файле содержаться слова, каждое слово на новой строчке.
        //После запуска программы должен создаться файл, который будет содержать только палиндромы

        File file = new File("/Users/sladyzhin/IdeaProjects/ver/dz/palindron.txt");
        FileReader fileReader = new FileReader(file);

        File resFile = new File("/Users/sladyzhin/IdeaProjects/ver/dz/Filepalindrom.txt");
        resFile.createNewFile();
        FileWriter fileWriter = new FileWriter(resFile);

        Scanner scanner = new Scanner(fileReader);

        while (scanner.hasNextLine()) {
            String row = scanner.nextLine();

            if (TextFormatter.isPalindrome(row)) {
                fileWriter.write(row + "\n");
            }
        }

        fileReader.close();
        fileWriter.close();
    }
}


