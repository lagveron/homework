package homework.lesson6;

public class TextFormatter {

    public static int countWordsNumberInString(String string) {
        int count = 0;
        String resString = string.trim();

        if (resString.length() != 0) {
            count++;
        }

        for (int i = 0; i < resString.length(); i++) {
            if (resString.charAt(i) == ' ') {
                count++;
            }
        }
        return count;
    }

    public static boolean hasPalindrome(String string) {

        String formattedString = string.replace(',', ' ');

        String[] wordsArray = formattedString.split(" ");

        for (int i = 0; i < wordsArray.length - 1; i++) {
            if (isPalindrome(wordsArray[i])) {
                return true;
            }
        }
        return false;
    }

    public static boolean isPalindrome(String string) {

        if (string.length() < 2) {
            return false;
        }

        int count = 0;
        for (int i = 0; i < string.length() / 2; i++) {
            if (string.charAt(i) == string.charAt(string.length() - 1 - i)) {
                count++;
            }
        }
        return count >= ((string.length() - 1) / 2);
    }
}

