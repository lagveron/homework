package homework.lesson6;

import java.util.Scanner;

public class Task1_Main {
    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);

        //Task 1- Вырезать подстроку из строки начиная с первого вхождения символа (А) до последнего вхождения символа (В)

        String string1 = scanner.nextLine();
        String formattedString1 = string1.substring(string1.indexOf("A"), string1.indexOf("B"));
        System.out.println(formattedString1);

        //Task 2 - Замените все вхождения символа, стоящего в позиции 3, на символ в позиции 0

        String string2 = scanner.nextLine();
        String formattedString2 = string2.replace(string2.charAt(3), string2.charAt(0));
        System.out.println(formattedString2);

    }
}
