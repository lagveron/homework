package homework.lesson6;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class TestFormatterMain {
    public static void main(String[] args) throws Exception {

        File file = new File("/Users/sladyzhin/IdeaProjects/ver/dz/text.txt");
        FileReader fileReader = new FileReader(file);

        File resFile = new File("/Users/sladyzhin/IdeaProjects/ver/dz/resText.txt");
        FileWriter fileWriter = new FileWriter(resFile);

        Scanner scanner = new Scanner(fileReader);

        while (scanner.hasNextLine()) {

            String row = scanner.nextLine();

            if (TextFormatter.countWordsNumberInString(row) >= 3 && TextFormatter.countWordsNumberInString(row) <= 5) {
                fileWriter.write(row + "\n");
            } else if (TextFormatter.hasPalindrome(row)) {
                fileWriter.write(row + "\n");
            }
        }
        fileReader.close();
        fileWriter.close();
    }
}
