package homework.lesson7;

import java.util.Random;

public class Car {

    private static final Random RANDOM = new Random();

    private String brand;
    private double speed;
    private double price;

    public Car() {
    }

    public Car(String brand, double speed, double price) {
        this.brand = brand;
        this.speed = speed;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void start() {
        if (RANDOM.nextInt(21) % 2 == 0) {
            throw new NotOddArgumentException("even number");
        } else {
            System.out.println("The car " + brand + " start ");
        }
    }
}
