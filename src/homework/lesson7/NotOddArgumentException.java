package homework.lesson7;

public class NotOddArgumentException extends RuntimeException {

    public NotOddArgumentException(String message) {
        super(message);
    }
}
