package homework.lesson7;

public class Main {

    public static void main(String[] args) {

        Car car1 = new Car("Porsche", 220.0, 50000.0);
        Car car2 = new Car("Ford", 210.0, 20000.0);
    }

    public void starter(Car car){
        try {
            car.start();
        } catch (NotOddArgumentException notOddArgumentException) {
            System.out.println("The car won't start");
        }
    }
}
