package homework.lesson1;

public class Task7 {

    public static void main(String[] args) {
        String programmersCount = "1110";

        if (programmersCount.endsWith("1")) {
            System.out.println(programmersCount + " программист");
        } else if (programmersCount.endsWith("2")
                || programmersCount.endsWith("3")
                ||programmersCount.endsWith("4")) {
            System.out.println(programmersCount + " программистa");
        } else {
            System.out.println(programmersCount + " программистов");
        }
    }
}
