package homework.lesson1;

public class Task3 {

    public static void main(String[] args) {
        int a = 20;

        if (a > 0) {
            a++;
        } else if (a < 0) {
            a -= 2;
        } else {
            a = 10;
        }
        System.out.println(a);
    }
}
