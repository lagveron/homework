package homework.lesson1;

public class Task4 {

    public static void main(String[] args) {

        int[] numbers = {2, -3, 0};
        int positivecounter = 0;

        for (int i = 0; i <= numbers.length - 1; i++) {
            if (numbers[i] > 0) {
                positivecounter++;
            }
        }

        System.out.println(positivecounter);
    }
}
