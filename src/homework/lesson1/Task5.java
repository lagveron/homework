package homework.lesson1;

public class Task5 {

    public static void main(String[] args) {


        int[] numbers = {2, -3, 0};
        int positiveCounter = 0;
        int negativeCounter = 0;

        for (int i = 0; i <= numbers.length - 1; i++) {
            if (numbers[i] > 0) {
                positiveCounter++;
            }

            if (numbers[i]<0){
                negativeCounter++;
            }

        }
        System.out.println("Count of positive numbers - " + positiveCounter);
        System.out.println("Count of negative numbers - " + negativeCounter);
    }
}
